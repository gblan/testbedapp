import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {
    path: 'search',
    loadChildren: () => import('testRemoteApp/Search').then(m => m.SearchModule)
  },
  {
    path: 'data',
    loadChildren: () => import('testRemoteApp/Data').then(m => m.DataModule)
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
